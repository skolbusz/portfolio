---
layout: default
modal-id: 7
date: 2020-07-20
img: wellezza.png
alt: Wellezza salon fryzjerski
project-date: Lipiec 2020
title: Wellezza
client: Wellezza
category: Strony internetowe
project_link: https://wellezza.pl/
description: Strona internetowa utworzona dla salonu fryzjerskiego w Wieliczce. Strona została stworzona przy użyciu najnowszych technologii- framework'ów GatsbyJS, TailwindCSS oraz oparta na platformie DatoCMS. Wszystkie kluczowe elementy na stronie można dowolnie edytować. Galeria zdjęć hostowana jest na rozwiązaniu Cloudinary. Cała strona jest w pełni responsywna oraz oferuje podstawową funkcjonalność również offline


---

