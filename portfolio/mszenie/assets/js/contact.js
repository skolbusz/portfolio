
$(function () {

    // Wywołanie walidatora

    $('#contact').validator();


    // W momencie wysłania formularza
    $('#contact').on('submit', function (e) {

        // Jeżeli walidator zezwolił na wysłanie formularza
        if (!e.isDefaultPrevented()) {
            var url = "assets/php/contact.php";

            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                success: function (data)
                {

                    // Otrzymujemy typy wiadomości: success x danger i stosujemy je
                    var messageAlert = 'alert-' + data.type;
                    var messageText = data.message;

                    // Pryzpisujuem wiadomość do alertu Bootstrap
                    var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
                    if (messageAlert && messageText) {
                        $('#contact').find('.messages').html(alertBox);
                        // Czyszczenie formularza
                        $('#contact')[0].reset();
                        grecaptcha.reset();
                    }
                }
            });
            return false;
        }
    })
});