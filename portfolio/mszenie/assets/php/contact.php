<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//use PHPMailer\PHPMailer\SMTP; //tylko na wypadek używania SMTP

require '../../vendor/PHPmailer/src/PHPMailer.php';
require '../../vendor/PHPmailer/src/Exception.php';
//require '../../vendor/PHPmailer/src/SMTP.php'; //tylko na wypadek używania SMTP
require('../../vendor/REcaptcha/autoload.php');

//SecretKey ReCaptcha
$secret = '6Le46DMUAAAAAC2ye1z4NCVyfcMU6ZFjziVxr5Nu';

// Adres email i imie i nazwisko pobrane z formularza
$fromEmail = $_POST['email'];
$fromName = $_POST['name'];

// Adres email i imię na które ma być wysłany formularz
$sendToEmail = 'biuro@artcoders.pl';
$sendToName = 'Biuro ArtCoders';

// Temat maila
$subject = $_POST['subject'];

// Nazwy pól formularza i ich odpowiedniki do maila
$fields = array('name' => 'Imię i nazwisko', 'email' => 'E-mail', 'subject' => 'Temat', 'content' => 'Wiadomość');

// Wiadomość wyświetlana po wysłaniu maila
$okMessage = 'Twoja wiadomość została wysłana pomyślnie, postaramy się odpowiedzieć tak szybko, jak tylko to będzie możliwe!';

// Wiadomość po niepowodzeniu.
$errorMessage = 'Wystąpił błąd podczas wysyłania wiadomości. Sprawdź wprowadzone dane bądź spróbuj ponownie później.';



// wyłączenie wyświetlania błędów (ma wpływ na headers)
error_reporting(0);

try
{
    if(!empty($_POST)) {

        //Walidowanie ReCaptchy

        if (!isset($_POST['g-recaptcha-response'])) {
            throw new \Exception('Nie uzupełniono ReCaptchy.');
        }

        $recaptcha = new \ReCaptcha\ReCaptcha($secret, new \ReCaptcha\RequestMethod\CurlPost());

        // Walidujemy ReCaptcha razem z adresem IP użytkownika

        $response = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);


        if (!$response->isSuccess()) {
            throw new \Exception('ReCaptcha niepoprawna.');
        }


        $emailTextHtml = "<h1>Masz nową wiadomość z formularza kontakowego</h1><hr>";
        $emailTextHtml .= "<table>";

        foreach ($_POST as $key => $value) {
            // Jeżeli pole występuje we wcześniej utworzonej tablicy, uwzględnij w mailu
            if (isset($fields[$key])) {
                $emailTextHtml .= "<tr><th>$fields[$key]</th><td>$value</td></tr>";
            }
        }
        $emailTextHtml .= "</table><hr>";
        $emailTextHtml .= "<p>Miłego dnia</p>";

        $mail = new PHPMailer;

        //W razie własnego SMTP

        /*
        $mail->isSMTP();
        $mail->SMTPDebug = 2;
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "domowmszenie@gmail.com";
        $mail->Password = "mszeniedomow";
        */

        $mail->setLanguage('pl', '../../vendor/PHPmailer/language/');

        $mail->setFrom($fromEmail, $fromName);
        $mail->addAddress($sendToEmail, $sendToName);
        $mail->addReplyTo($fromEmail);

        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->msgHTML($emailTextHtml);

        if (!$mail->send()) {
            throw new \Exception('Uwaga. ' . $mail->ErrorInfo);
        }

        $responseArray = array('type' => 'success', 'message' => $okMessage);
    }
}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $e->getMessage());
}

// Odpowiedź JSON do wyświetlenia komunikatu
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);

    header('Content-type:application/json;charset=utf-8');

    echo $encoded;
}
else {
    echo $responseArray['message'];
}